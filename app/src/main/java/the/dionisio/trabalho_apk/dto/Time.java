package the.dionisio.trabalho_apk.dto;

/**
 * Created by igorm on 11/04/2017.
 */

public class Time {
    public Integer quantidadeVitoria;
    public Integer quantidadeEmpate;
    public Integer totalPontos;
    public Integer quantidadeGols;
    public Float classificacao;
    public String posicao;
}
