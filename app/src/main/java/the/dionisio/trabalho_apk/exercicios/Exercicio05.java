package the.dionisio.trabalho_apk.exercicios;

/**
 * Created by igorm on 11/04/2017.
 */

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import the.dionisio.trabalho_apk.R;
import the.dionisio.trabalho_apk.util.Util;

public class Exercicio05 extends AppCompatActivity {

    private EditText edtCoordenada;
    private Button btnCalcularEx05;
    private TextView txtResultadoEx05;
    private ImageButton btnBackMenuEx05, btnResetEx05;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bar_exercicio05);

        edtCoordenada = (EditText) findViewById(R.id.edtCoordenada);
        txtResultadoEx05 = (TextView) findViewById(R.id.txtResultadoEx05);

        btnCalcularEx05 = (Button) findViewById(R.id.btnCalcularEx05);
        btnBackMenuEx05 = (ImageButton) findViewById(R.id.btnBackMenuEx05);
        btnResetEx05 = (ImageButton) findViewById(R.id.btnResetEx05);
    }

    public void calcularEx05(View v)
    {
        try
        {
            txtResultadoEx05.setText(edtCoordenada.getText());
            String [] splited = String.valueOf(edtCoordenada.getText()).split(",");
            String result ="";
            Integer x = Integer.parseInt(splited[0]);
            Integer y = Integer.parseInt(splited[1]);

            if (x<432 && y<468 && y>-1 && x>-1)
            {
                if (x>430 | y>466)
                {
                    result=" ➤ Caiu na faixa !";
                }
                else
                {
                    result=" ➤ Caiu na quadra !";
                }
            }
            else
            {
                result=" ➤ Caiu fora !";
            }

            txtResultadoEx05.setText(result);

            edtCoordenada.setEnabled(false);
            btnCalcularEx05.setEnabled(false);

            btnBackMenuEx05.setVisibility(View.VISIBLE);
            btnResetEx05.setVisibility(View.VISIBLE);
        }
        catch (Exception err)
        {
            edtCoordenada.setText("");
            txtResultadoEx05.setText("Você como primata que é, não foi capaz de inserir um coordenada valida");
        }
    }

    public void backMenuEx05(View v)
    {
        Util.moviment.back(this);
    }

    public void refazerEx05(View v){
        Util.refactor.refactor(this);
    }
}
