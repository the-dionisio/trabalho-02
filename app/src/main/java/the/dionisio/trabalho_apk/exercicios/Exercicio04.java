package the.dionisio.trabalho_apk.exercicios;

/**
 * Created by igorm on 11/04/2017.
 */

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import the.dionisio.trabalho_apk.R;
import the.dionisio.trabalho_apk.util.Util;

public class Exercicio04 extends AppCompatActivity {

    private EditText edtTamanhoChocolate;
    private Button btnCalcularEx04;
    private TextView txtResultadoEx04;
    private ImageButton btnBackMenuEx04, btnResetEx04;
    private Integer numero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bar_exercicio04);

        edtTamanhoChocolate = (EditText) findViewById(R.id.edtTamanhoChocolate);
        txtResultadoEx04 = (TextView) findViewById(R.id.txtResultadoEx04);

        btnCalcularEx04 = (Button) findViewById(R.id.btnCalcularEx04);
        btnBackMenuEx04 = (ImageButton) findViewById(R.id.btnBackMenuEx04);
        btnResetEx04 = (ImageButton) findViewById(R.id.btnResetEx04);
    }

    public void calcularEx04(View v)
    {
        try
        {
            //Aqui vai o seu codigo que vai realizar o calculo;
            numero = Util.validationField.validation(
                    getApplicationContext(), numero, edtTamanhoChocolate);

            if(numero == 0)
            {
                Toast.makeText(getApplicationContext() ,
                        "Favor inserir um número inteiro maior que zero!",
                        Toast.LENGTH_SHORT).show();
            }
            else
            {
                int pedaco = 1;
                while(numero >= 2){
                    numero = numero /2;
                    pedaco *= 4;
                }

                //Aqui você seta o resultado na tela através de um txt;
                txtResultadoEx04.setText("Quantidade de pedaços: " + pedaco);

                //Aqui bloqueia os campos do exercício para que a
                //pessoa escolha  voltar ao menu ou refazer o exercício;
                edtTamanhoChocolate.setEnabled(false);
                btnCalcularEx04.setEnabled(false);

                //Disponibiliza as opções de voltar ao menu ou refazer
                //o exercício;
                btnBackMenuEx04.setVisibility(View.VISIBLE);
                btnResetEx04.setVisibility(View.VISIBLE);
            }
        }
        catch (Exception err)
        {
        }
    }

    public void backMenuEx04(View v)
    {
        Util.moviment.back(this);
    }

    public void refazerEx04(View v){
        Util.refactor.refactor(this);
    }
}
