package the.dionisio.trabalho_apk.exercicios;

/**
 * Created by igorm on 11/04/2017.
 */

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import the.dionisio.trabalho_apk.R;
import the.dionisio.trabalho_apk.util.Util;

public class Exercicio02 extends AppCompatActivity {

    private EditText edtSequencia;
    private Button btnAdicionarEx02;
    private TextView txtSequencia, txtMaiorNumero;
    private ImageButton btnBackMenuEx02, btnResetEx02;
    private Integer numero = 1, maior_numero = Integer.MIN_VALUE;
    private ArrayList<Integer> listNum = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bar_exercicio02);

        edtSequencia = (EditText) findViewById(R.id.edtSequencia);

        btnAdicionarEx02 = (Button) findViewById(R.id.btnAdicionarEx02);

        txtSequencia = (TextView) findViewById(R.id.txtSequencia);
        txtMaiorNumero = (TextView) findViewById(R.id.txtMaiorNumero);

        btnBackMenuEx02 = (ImageButton) findViewById(R.id.btnBackMenuEx02);
        btnResetEx02 = (ImageButton) findViewById(R.id.btnResetEx02);
    }

    public void calcularEx02(View v)
    {
        try
        {
            numero = Util.validationField.validation(getApplicationContext(), numero, edtSequencia);
            if(!edtSequencia.getText().toString().equals("0"))
            {
                //Aqui vai o seu codigo que vai realizar o calculo;
                edtSequencia.setText("");
                listNum.add(numero);
                txtSequencia.setText("Sequência: " + listNum.toString());

                if(numero > maior_numero)
                {
                    maior_numero = numero;
                }
            }
            else
            {
                //Aqui você seta o resultado na tela através de um txt;
                txtSequencia.setVisibility(View.INVISIBLE);
                txtMaiorNumero.setText("Maior Número: " + maior_numero);
                listNum.clear();
                maior_numero = 0;
                txtSequencia.setText("");
                //Aqui bloqueio os campos do exercício para que a
                //pessoa escolha  voltar ao menu ou refazer o exercício;
                edtSequencia.setEnabled(false);
                btnAdicionarEx02.setEnabled(false);

                //Disponibiliza as opções de voltar ao menu ou refazer
                //o exercício;
                btnBackMenuEx02.setVisibility(View.VISIBLE);
                btnResetEx02.setVisibility(View.VISIBLE);
            }
        }
        catch (Exception err)
        {
        }
    }

    public void backMenuEx02(View v)
    {
        Util.moviment.back(this);
    }

    public void refazerEx02(View v){
        Util.refactor.refactor(this);
    }
}
