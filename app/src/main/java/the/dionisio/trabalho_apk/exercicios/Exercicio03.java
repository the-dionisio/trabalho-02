package the.dionisio.trabalho_apk.exercicios;

/**
 * Created by igorm on 11/04/2017.
 */

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

import the.dionisio.trabalho_apk.R;
import the.dionisio.trabalho_apk.dto.Time;
import the.dionisio.trabalho_apk.util.Util;

public class Exercicio03 extends AppCompatActivity {

    private EditText edtQuantidadeTime, edtQuantidadeVitoria, edtQuantidadeEmpate, edtQuantidadeGol;
    private Button btnContinuarEx03, btnAdicionarEx03, btnFinalizarEx03;
    private TextView txtInformacaoTimeEx03;
    private ListView listTime;
    private ArrayAdapter<String> adpTime;
    private List<Time> times = new ArrayList<>();
    private Integer quantidadeTime, controle = 1;
    private ImageButton btnBackMenuEx03, btnResetEx03;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bar_exercicio03);

        edtQuantidadeTime = (EditText) findViewById(R.id.edtQuantidadeTime);
        edtQuantidadeVitoria = (EditText) findViewById(R.id.edtQuantidadeVitoria);
        edtQuantidadeEmpate = (EditText) findViewById(R.id.edtQuantidadeEmpate);
        edtQuantidadeGol = (EditText) findViewById(R.id.edtQuantidadeGol);

        btnContinuarEx03 = (Button) findViewById(R.id.btnContinuarEx03);
        btnAdicionarEx03 = (Button) findViewById(R.id.btnAdicionarEx03);
        btnFinalizarEx03 = (Button) findViewById(R.id.btnFinalizarEx03);

        txtInformacaoTimeEx03 = (TextView) findViewById(R.id.txtInformacaoTimeEx03);
        listTime = (ListView) findViewById(R.id.listTime);

        adpTime = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        listTime.setAdapter(adpTime);

        btnBackMenuEx03 = (ImageButton) findViewById(R.id.btnBackMenuEx03);
        btnResetEx03 = (ImageButton) findViewById(R.id.btnResetEx03);
    }

    public void continuarEx03(View v)
    {
        try
        {
            quantidadeTime =
                    Util.validationField.validation(getApplicationContext(),
                            quantidadeTime, edtQuantidadeTime);

            if(quantidadeTime == 0)
            {
                Toast.makeText(getApplicationContext() , "Favor inserir um número inteiro maior que zero!", Toast.LENGTH_SHORT).show();
            }
            else
            {
                edtQuantidadeTime.setVisibility(View.INVISIBLE);
                btnContinuarEx03.setVisibility(View.INVISIBLE);

                edtQuantidadeVitoria.setVisibility(View.VISIBLE);
                edtQuantidadeEmpate.setVisibility(View.VISIBLE);
                edtQuantidadeGol.setVisibility(View.VISIBLE);
                txtInformacaoTimeEx03.setVisibility(View.VISIBLE);
                if(quantidadeTime == 1)
                {
                    btnFinalizarEx03.setVisibility(View.VISIBLE);
                }
                else
                {
                    btnAdicionarEx03.setVisibility(View.VISIBLE);
                }

                txtInformacaoTimeEx03.setText("INFORMAÇÕES DO 1º TIME");
            }
        }
        catch (Exception err)
        {
        }
    }

    public Time preencherLista(Time time)
    {
        time.quantidadeVitoria =
                Util.validationField.validation(getApplicationContext(),
                        time.quantidadeVitoria, edtQuantidadeVitoria);
        time.quantidadeEmpate =
                Util.validationField.validation(getApplicationContext(),
                        time.quantidadeEmpate, edtQuantidadeEmpate);
        time.quantidadeGols =
                Util.validationField.validation(getApplicationContext(),
                        time.quantidadeGols, edtQuantidadeGol);

        time.totalPontos = ((time.quantidadeVitoria*3) + (time.quantidadeEmpate));
        time.classificacao = Float.parseFloat(time.totalPontos + "." + time.quantidadeGols);
        times.add(time);
        return time;
    }

    public void adicionarTime(View v)
    {
        try
        {
            Time time = new Time();
            time = preencherLista(time);

            quantidadeTime = quantidadeTime -1;

            if(quantidadeTime !=1)
            {
                edtQuantidadeVitoria.setText("");
                edtQuantidadeEmpate.setText("");
                edtQuantidadeGol.setText("");

                edtQuantidadeVitoria.requestFocus();
            }
            else
            {
                edtQuantidadeVitoria.setText("");
                edtQuantidadeEmpate.setText("");
                edtQuantidadeGol.setText("");

                edtQuantidadeVitoria.requestFocus();

                btnAdicionarEx03.setVisibility(View.INVISIBLE);
                btnFinalizarEx03.setVisibility(View.VISIBLE);
            }
            controle++;
            txtInformacaoTimeEx03.setText("INFORMAÇÕES DO " +(controle)+ "º TIME");
        }
        catch (Exception err)
        {
            Toast.makeText(getApplicationContext() , "Favor informar todos os campos!", Toast.LENGTH_SHORT).show();
        }
    }

    public void finalizarClassificacao(View v)
    {
        try
        {
            if(quantidadeTime == 1)
            {
                Time time = new Time();
                time = preencherLista(time);
            }

            Collections.sort(times, (o1, o2) -> o2.classificacao.compareTo(o1.classificacao));

            for (int i= 0; i< times.size(); i++)
            {
                times.get(i).posicao = (i+1) + "º LUGAR";
                adpTime.add(times.get(i).posicao +
                        " - Pontos: " + times.get(i).totalPontos +
                        " - Vitórias: " + times.get(i).quantidadeVitoria +
                        " - Empates: " + times.get(i).quantidadeEmpate +
                        " - Gols: " + times.get(i).quantidadeGols);
            }

            edtQuantidadeVitoria.setVisibility(View.INVISIBLE);
            edtQuantidadeEmpate.setVisibility(View.INVISIBLE);
            edtQuantidadeGol.setVisibility(View.INVISIBLE);
            btnFinalizarEx03.setVisibility(View.INVISIBLE);

            txtInformacaoTimeEx03.setText("Classificação");
            listTime.setVisibility(View.VISIBLE);

            btnBackMenuEx03.setVisibility(View.VISIBLE);
            btnResetEx03.setVisibility(View.VISIBLE);
        }
        catch (Exception err)
        {
            Toast.makeText(getApplicationContext() , "Favor informar todos os campos!", Toast.LENGTH_SHORT).show();
        }
    }

    public void backMenuEx03(View v)
    {
        Util.moviment.back(this);
    }

    public void refazerEx03(View v){
        Util.refactor.refactor(this);
    }
}
