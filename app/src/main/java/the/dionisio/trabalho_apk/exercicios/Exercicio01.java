package the.dionisio.trabalho_apk.exercicios;

/**
 * Created by igorm on 11/04/2017.
 */

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import the.dionisio.trabalho_apk.R;
import the.dionisio.trabalho_apk.util.Util;

public class Exercicio01 extends AppCompatActivity {

    private EditText edtQuantidadePessoas;
    private Button btnCalcularEx01;
    private TextView txtResultadoEx01;
    private ImageButton btnBackMenuEx01, btnResetEx01;
    private Integer numero, numero2, numero3;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bar_exercicio01);

        edtQuantidadePessoas = (EditText) findViewById(R.id.edtQuantidadePessoas);
        txtResultadoEx01 = (TextView) findViewById(R.id.txtResultadoEx01);

        btnCalcularEx01 = (Button) findViewById(R.id.btnCalcularEx01);
        btnBackMenuEx01 = (ImageButton) findViewById(R.id.btnBackMenuEx01);
        btnResetEx01 = (ImageButton) findViewById(R.id.btnResetEx01);
    }

    public void calcularEx01(View v)
    {
        try
        {
            //Aqui vai o seu codigo que vai realizar o calculo;
            numero = Util.validationField.validation(
                    getApplicationContext(), numero, edtQuantidadePessoas);

            if(numero == 0)
            {
                Toast.makeText(getApplicationContext() , "Favor inserir um número inteiro maior que zero!", Toast.LENGTH_SHORT).show();
            }
            else
            {
                numero2 = 2*numero;
                numero3 = 2*numero2;
                //Aqui você seta o resultado na tela através de um txt;
                txtResultadoEx01.setText("Link 1 :" + numero3);
                //Aqui bloqueia os campos do exerc ício para que a
                //pessoa escolha  voltar ao menu ou refazer o exercício;
                edtQuantidadePessoas.setEnabled(false);
                btnCalcularEx01.setEnabled(false);

                //Disponibiliza as opções de voltar ao menu ou refazer
                //o exercício;
                btnBackMenuEx01.setVisibility(View.VISIBLE);
                btnResetEx01.setVisibility(View.VISIBLE);
            }
        }
        catch (Exception err)
        {
        }
    }

    public void backMenuEx01(View v)
    {
        Util.moviment.back(this);
    }

    public void refazerEx01(View v){
        Util.refactor.refactor(this);
    }
}
