package the.dionisio.trabalho_apk.util;

import android.app.Activity;
import android.content.Intent;

/**
 * Created by igorm on 13/04/2017.
 */

public class Refactor
{
    public void refactor(Activity activity)
    {
        Intent intent=new Intent();
        intent.setClass(activity, activity.getClass());
        //reiniciar a aticvity nova
        activity.startActivity(intent);
        //encerra a ativity atual
        activity.finish();
    }
}
