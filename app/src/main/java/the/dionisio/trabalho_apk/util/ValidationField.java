package the.dionisio.trabalho_apk.util;

import android.content.Context;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by igorm on 14/04/2017.
 */

public class ValidationField
{
    public Integer validation(Context context, Integer field, EditText editText)
    {
        try
        {
            field = Integer.parseInt(editText.getText().toString());
            return field;
        }
        catch (Exception err)
        {
            Toast.makeText(context , "Favor inserir um número inteiro!", Toast.LENGTH_SHORT).show();
             field = Integer.parseInt("");
            return field;
        }
    }
}
