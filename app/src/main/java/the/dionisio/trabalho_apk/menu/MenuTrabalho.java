package the.dionisio.trabalho_apk.menu;

/**
 * Created by igorm on 11/04/2017.
 */

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import the.dionisio.trabalho_apk.R;
import the.dionisio.trabalho_apk.exercicios.Exercicio01;
import the.dionisio.trabalho_apk.exercicios.Exercicio02;
import the.dionisio.trabalho_apk.exercicios.Exercicio03;
import the.dionisio.trabalho_apk.exercicios.Exercicio04;
import the.dionisio.trabalho_apk.exercicios.Exercicio05;
import the.dionisio.trabalho_apk.util.Util;

public class MenuTrabalho extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TextView txtInformacaoEx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_trabalho);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        txtInformacaoEx = (TextView) findViewById(R.id.txtInformacaoEx);
        txtInformacaoEx.setText("TRABALHO [ 2 ] LINGUAGEM DE PROGRAMAÇÃO PARA DISPOSITIVOS MÓVEIS " +
                        "\nTrabalho para treino de lógica, usando java + android, e, quando possível implementamos práticas do java 8." +
                        "\n\nLISTA DE EXERÍCIOS" +
                        "\n\n1) João fez uma pesquisa em seu site de busca predileto, e encontrou a resposta que estava procurando no terceiro link listado. Além disso, ele viu," +
                        " pelo site, que x pessoas já haviam clicado neste link antes. João havia lido anteriormente, também na Internet, que o número de pessoas que" +
                        " clicam no segundo link listado é o dobro de número de pessoas que clicam no terceiro link listado. Nessa leitura, ele também descobriu que o número" +
                        " de pessoas que clicam no segundo link é a metade do número de pessoas que licam no primeiro link. João está intrigado para saber quantas pessoas clicaram" +
                        " no rimeiro link da busca, e, como você é amigo dele, quer sua ajuda nesta tarefa." +
                        "\nEntrada " +
                        "\nApenas um número, x, que representa o número de pessoas que clicaram no terceiro link da busca." +
                        "\nSaída" +
                        "\nMostre apenas um inteiro, indicando quantas pessoas clicaram no primeiro link, nessa busca. " +
                        "\n\n2) Leonardo é um garoto muito criativo. Ele adora criar desafios para seus colegas da escola. Seu último desafio é o seguinte: diversos números são ditos em voz" +
                        " alta, quando o número 0 (zero) é dito então o desafio termina e seus colegas devem dizer imediatamente qual foi o maior número. Leonardo tem muita dificuldade de" +
                        " verificar se a resposta dada pelos colegas é correta ou não, pois a sequência de números costuma ser longa. Por este motivo, ele resolveu pedir sua ajuda. Sua tarefa" +
                        " é escrever um programa que dada uma sequência de números inteiros positivos terminada por 0 (zero), imprime o maior número da sequência." +
                        "\nEntrada" +
                        "\nUma sequência de números inteiros positivos diferentes de zero, pois o zero representa o término dos números da sequência." +
                        "\nSaída " +
                        "\nMostrar o maior número dentre os números da entrada. " +
                        "\n\n3) Foi montado um campeonato de futebol com diversos times. A pontuação para identificar o time vitorioso segue as seguintes regras: Cada vitória conta três pontos" +
                        " Cada empate um ponto. " +
                        "\nFica melhor classificado no campeonato o time que tenha mais pontos. Em caso de empate no número de pontos, fica melhor classificado o time que tiver maior saldo" +
                        " de gols. Se o número de pontos e o saldo de gols forem os mesmos para mais de um time, então eles estão empatados no campeonato. Dados os números de vitórias e empates," +
                        " e os saldos de gols dos times, sua tarefa é determinar a classificação final dos times." +
                        "\nEntrada" +
                        "\nDeverá ser informado o número de times participantes. Para cada time informar : Número de vitórias Número de empates * Saldo de gols + " +
                        "\nSaída " +
                        "Mostrar a relação de times por ordem decrescente de classificação" +
                        "\n\n4) Foi definido que todas as barras de chocolate são quadradas. Ana Maria tem uma barra quadrada de chocolate de lado L, e quer compartilhar com alguns colegas. " +
                        "Como ela é muito honesta, então, ela divide a barra em quatro pedaços quadrados, de lado L/2. Depois, ela repete esse procedimento com cada pedaço gerado, sucessivamente," +
                        " enquanto o lado for maior do que, ou igual a 2cm. Você deve escrever um programa que, dado o lado L da barra inicial, em centímetros, determina quantos pedaços haverá ao" +
                        " final do processo. " +
                        "\nEntrada" +
                        "\nA entrada em um único inteiro, L, que corresponde ao número de centímetros do lado do quadrado." +
                        "\nSaída" +
                        "\nO programa deve imprimir um único inteiro que representa o número total de pedaços obtidos." +
                        "\n\n5) Uma quadra de tênis tem o formato de um retângulo, cujos lados medem 432 polegadas por 936 polegadas, tendo 1,97 polegadas a largura das faixas das bordas." +
                        " Em um Grand Slam na Austrália, Rafael Nadal perdeu para Novak Djokovic, num dos jogos mais bonitos de tênis.Muitas vezes, uma jogada é tão rápida, e a bola tão próxima" +
                        " da borda da quadra, que o juiz pode tomar uma decisão que pode ser contestada por um dos jogadores. Para isso, existe o desafio, que utiliza a imagem gravada do jogo" +
                        " para decidir se a bola estava dentro ou fora da metade da quadra correspondente a um dos jogadores. Considere que a semi-quadra de Rafael Nadal corresponde a um retângulo" +
                        " em que dois vértices têm coordenadas (0,0) e (432, 468), onde todos os números são em polegadas. Você deve escrever um programa para, dadas as coordenadas (X; Y ) do ponto" +
                        " de ontato da bola com o solo, determinar se uma bola bateu no solo: Dentro da semi-quadra Fora da semi-quadra. * Na linha da semi-quadra" +
                        "\nEntrada" +
                        "\nA entrada contém dois inteiros X e Y , que correspondem às coordenadas do ponto (X; Y ) de contato da bola com o solo, em polegadas." +
                        "\nSaída" +
                        "\nO programa deve imprimir a palavra: " +
                        "\ndentro se a bola bateu dentro da semi-quadra, "+
                        "\nfora se a bola bateu fora da semi-quadra" +
                        "\nlinha se a bola bateu na linha da semi-quadra");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_trabalho, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_ex01) {
            Util.moviment.go(this, Exercicio01.class);
        } else if (id == R.id.nav_ex02) {
            Util.moviment.go(this, Exercicio02.class);
        } else if (id == R.id.nav_ex03) {
            Util.moviment.go(this, Exercicio03.class);
        } else if (id == R.id.nav_ex04) {
            Util.moviment.go(this, Exercicio04.class);
        } else if (id == R.id.nav_ex05) {
            Util.moviment.go(this, Exercicio05.class);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
